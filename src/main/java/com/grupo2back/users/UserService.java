package com.grupo2back.users;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;
import org.apache.catalina.User;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.css.DocumentCSS;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.set;

public class UserService {

    static MongoCollection<Document> usuarios;

    private static MongoCollection<Document> getUsersCollection() {
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        MongoClient mongo = MongoClients.create(settings);
        MongoDatabase database = mongo.getDatabase("dbmovibbva");

        return database.getCollection("users");
    }

    private static Boolean ifUserExist(String userName) {
        boolean isExist = true;
        usuarios =  getUsersCollection();
        List lst = new ArrayList();
        FindIterable<Document> iterDoc = usuarios.find(new Document("userName",userName));
        Iterator it =iterDoc.iterator();

        while (it.hasNext()){
            lst.add(it.next());
        }

        if(lst.isEmpty()) isExist = false ;

        return isExist;
    }

    public static void insertUser(String strUser) throws UserException {
        usuarios = getUsersCollection();
        List lstUsers = getUsers();
        Document doc = Document.parse(strUser);
        doc.append("idUser", lstUsers.size() + 1);
        boolean ifExist = ifUserExist(doc.getString("userName"));
        if(ifExist){
            throw new UserException();
        } else {
            usuarios.insertOne(doc);
        }
    }

    public static Document getUserById(String strId) throws UserException{
        usuarios =  getUsersCollection();

        Document doc = usuarios.find(eq("idUser", Integer.parseInt(strId))).first();

        if (doc == null) {
            throw new UserException();
        }

        return doc;
    }

    public static List getUsers() {
        usuarios =  getUsersCollection();

        List lst = new ArrayList();
        FindIterable<Document> iterDoc = usuarios.find();

        Iterator it = iterDoc.iterator();
        while(it.hasNext()) {
            lst.add(it.next());
        }

        return lst;
    }

    public static List login(String strUser) {
        usuarios =  getUsersCollection();
        List lst = new ArrayList();
        Document docUser = Document.parse(strUser);
        Document doc = usuarios.find(eq("userName", docUser.getString("userName"))).first();

        if (docUser.getString("password").equals(doc.getString("password"))) {
            Bson filter = eq("userName", docUser.getString("userName"));
            Bson update = set("logged", true);
            usuarios.updateOne(filter,update);
            lst.add(doc);
        }

        return lst;
    }

    public static void updateUser(String id, String strUser) throws UserException {
        usuarios =  getUsersCollection();
        Document user = getUserById(id);

        Document doc = new Document("$set", Document.parse(strUser));

        System.out.println("USEEEEEEEEER" + user);
        System.out.println("DPPPPPPPPPPp"+ doc);
        usuarios.updateOne(user, doc);
    }
}
