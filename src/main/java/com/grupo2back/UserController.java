package com.grupo2back;

import com.grupo2back.users.UserService;
import org.apache.catalina.User;
import org.bson.Document;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @PostMapping("/user")
    public ResponseEntity<Object> insertUser(@RequestBody String strUser) {
        try {
            UserService.insertUser(strUser);

            return  ResponseEntity.status(HttpStatus.OK).body(Map.of(
                    "Response", "Alta Correcta",
                    "Code", "200"
            ));
        } catch (Exception ex) {
            return  ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(Map.of(
                    "Response", "Error En el Alta (Revisar los Datos)",
                    "Code", "500"
            ));
        }
    }

    @GetMapping(path = "/user/{id}")
    public ResponseEntity<Object> getUser(@PathVariable String id){
        try {
            Document document = UserService.getUserById(id);
            return  ResponseEntity.status(HttpStatus.OK).body(document);
        } catch (Exception ex){
            return  ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(Map.of(
                    "Response", "Usuario no Encontrado",
                    "Code", "500"
            ));
        }
    }

    @GetMapping(path = "/")
    public ResponseEntity<Object> getAllUsers(){
        try {
            List lstUser =  UserService.getUsers();
            return  ResponseEntity.status(HttpStatus.OK).body(lstUser);

        } catch (Exception ex) {
            return  ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(Map.of(
                    "Response", "Usuarios no Encontrados",
                    "Code", "500"
            ));
        }
    }

    @PostMapping(path = "/user/login")
    @CrossOrigin(origins = "*")
    public ResponseEntity<Object> login(@RequestBody String strUser) {
        try {
            List lstUserLogged = UserService.login(strUser);
            return  ResponseEntity.status(HttpStatus.OK).body(lstUserLogged);
        } catch (Exception ex) {
            return  ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(Map.of(
                    "Response", "Usuario o Contraseña incorrecta",
                    "Code", "500"
            ));
        }
    }

    @PutMapping(path = "/user/updUser/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable String id, @RequestBody String user) {
        try {
            UserService.updateUser(id, user);
            return  ResponseEntity.status(HttpStatus.OK).body(Map.of(
                    "Response", "Modificación Correcta",
                    "Code", "200"
            ));
        } catch (Exception ex) {
            return  ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(Map.of(
                    "Response", "Fallo Modificación",
                    "Code", "500"
            ));
        }
    }

}
