package com.grupo2back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Grupo2BackRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(Grupo2BackRestApplication.class, args);
	}

}
