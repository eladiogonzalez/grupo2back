package com.grupo2back.saldos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SaldoServiceEst {
    @Autowired
    SaldoRepository saldoRepository;

    public List<Saldo> findAll(){
        return saldoRepository.findAll();
    }

    public Optional<Saldo> findById(String id){
        return saldoRepository.findById(id);
    }

    public Saldo save (Saldo sal){
        return saldoRepository.save(sal);
    }

    public boolean delete (Saldo sal){
        try{
            saldoRepository.delete(sal);
            return true;
        }catch (Exception ex){
            return false;
        }
    }
}
