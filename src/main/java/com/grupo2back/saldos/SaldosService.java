package com.grupo2back.saldos;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class SaldosService {
    static MongoCollection<Document> saldos;

    private static MongoCollection<Document> getSaldosCollection(){
        ConnectionString cs=new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        MongoClient mongo = MongoClients.create(settings);
        MongoDatabase database = mongo.getDatabase("dbmovibbva");
        return database.getCollection("saldos");
    }

    public static void insert(String strSaldos) throws Exception{
        saldos=getSaldosCollection();

        Document doc=Document.parse(strSaldos);
        List<Document> lst =doc.getList("saldos", Document.class);
        if(lst==null){
            saldos.insertOne(doc);
        }else{
            saldos.insertMany(lst);
        }
    }

/*    public static void updSaldos(String strSaldos,String strFiltro) throws Exception{
        double dblSaldoAct =0;
        Date fechaActual = new Date();
        Object oDbl =new Object();
        SimpleDateFormat objSDF=new SimpleDateFormat("dd-MM-aaaa");

        saldos=getSaldosCollection();

        Document doc=Document.parse(strSaldos);
        List<Document> lst =doc.getList("saldos", Document.class);
        if(lst==null){
            oDbl=doc.get("saldoActual");
            if (oDbl instanceof Number){
                dblSaldoAct=((Number) oDbl).doubleValue();
            }
            dblSaldoAct= dblSaldoAct+0;
            doc.put("saldoActual", dblSaldoAct);
            doc.put("fechaUltimoMovimiento", "22-04-2021");
            saldos.insertOne(doc);
        }else{
            saldos.insertMany(lst);
        }
    }*/

    public static List getFiltrado(String filtro){
        saldos=getSaldosCollection();
        List lst =new ArrayList();
        Document docFiltro=Document.parse(filtro);
        FindIterable<Document> iterDoc =saldos.find(docFiltro);
        Iterator it = iterDoc.iterator();
        while (it.hasNext()){
            lst.add(it.next());
        }
        return lst;
    }


    public static List getAll(){
        saldos=getSaldosCollection();
        List lst=new ArrayList();
        FindIterable<Document> iterDoc= saldos.find();
        Iterator it= iterDoc.iterator();
        while (it.hasNext()){
            lst.add(it.next());
        }
        return lst;
    }

    public static void update(String filtro, String updates){
        saldos =getSaldosCollection();
        Document docFiltro=Document.parse(filtro);
        Document doc=Document.parse(updates);
        saldos.updateOne(docFiltro, doc);
    }

    /*public static void update3(String usuario, String Monto){   // A validar, Filtro:usuario, updtes: monto movimiento
        double dblSaldoAct;
        double dblSaldoNvo;
        double dblMontoMov;

        Date fechaActual = new Date();
        SimpleDateFormat objSDF=new SimpleDateFormat("dd-MM-aaaa");

        saldos =getSaldosCollection();
        Document docFiltro=Document.parse(usuario);
        Document doc=Document.parse(Monto);

        dblSaldoAct= getSaldo("SaldoActual", usuario);  //Saldo Actual de usuario
        dblMontoMov= obj2Dbl(doc.get("monto2"));  //Monto Movimiento
        dblSaldoNvo= dblSaldoAct+dblMontoMov;     //Nuevo Saldo
        doc.put("SaldoActual", dblSaldoNvo);
        doc.put("fechaUltimoMovimiento", "2019-01-01");

        saldos.updateOne(docFiltro, doc);
    }

    public static void update(String filtro, String updates){   // A validar, Filtro:usuario, updtes: monto movimiento
        double dblSaldoAct;
        double dblSaldoNvo;
        double dblMontoMov;

        Date fechaActual = new Date();
        SimpleDateFormat objSDF=new SimpleDateFormat("dd-MM-aaaa");

        saldos =getSaldosCollection();
        Document docFiltro=Document.parse(filtro);
        Document doc=Document.parse(updates);


        dblSaldoAct= getSaldo("SaldoActual", filtro);  //Saldo Actual de usuario
        dblMontoMov= obj2Dbl(doc.get("monto"));  //Monto Movimiento
        dblSaldoNvo= dblSaldoAct+dblMontoMov;     //Nuevo Saldo
        doc.put("SaldoActual", dblSaldoNvo);
        doc.put("fechaUltimoMovimiento", "2019-01-01");

        saldos.updateOne(docFiltro, doc);
    }*/

    public static boolean existeSaldoUsurio(String strFiltro ){
        saldos=getSaldosCollection();
        List lst =new ArrayList();
        Document docFiltro=Document.parse(strFiltro);
        FindIterable<Document> iterDoc =saldos.find(docFiltro);
        Iterator it = iterDoc.iterator();
        while (it.hasNext()){
            lst.add(it.next());
        }
        if (lst == null){
            return true;
        }else return false;
    }

    public  static double getSaldo(String strTipoSaldo, String strFiltro){

        saldos=getSaldosCollection();
        List lst =new ArrayList();
        Document docFiltro=Document.parse(strFiltro);

        return obj2Dbl(docFiltro.get(strTipoSaldo));
    }

    public static double obj2Dbl (Object oMonto){
        double dblMonto=0;
        if (oMonto instanceof Number){
            dblMonto=((Number) oMonto).doubleValue();
        }
        return dblMonto;
    }


}
