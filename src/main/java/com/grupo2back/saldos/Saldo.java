package com.grupo2back.saldos;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "saldos")
public class Saldo {

    @Id
    private String idUsuario;
    private double saldoInicial;
    private double saldoActual;
    private String fechaUltimoMovimiento;

    public Saldo() {
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public double getSaldoInicial() {
        return saldoInicial;
    }

    public void setSaldoInicial(double saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    public double getSaldoActual() {
        return saldoActual;
    }

    public void setSaldoActual(double saldoActual) {
        this.saldoActual = saldoActual;
    }

    public String getFechaUltimoMovimiento() {
        return fechaUltimoMovimiento;
    }

    public void setFechaUltimoMovimiento(String fechaUltimoMovimiento) {
        this.fechaUltimoMovimiento = fechaUltimoMovimiento;
    }

    @Override
    public String toString() {
        return "Saldo{" +
                "idUsuario='" + idUsuario + '\'' +
                ", saldoInicial=" + saldoInicial +
                ", saldoActual=" + saldoActual +
                ", fechaUltimoMovimiento='" + fechaUltimoMovimiento + '\'' +
                '}';
    }
}
