package com.grupo2back.saldos;

public class FiltroSaldo {
    String idUsuario;

    public FiltroSaldo(String idUsr) {
        this.idUsuario = idUsr;
    }

    public FiltroSaldo() {
    }

    public String getIdUsr() {
        return idUsuario;
    }

    public void setIdUsr(String idUsr) {
        this.idUsuario = idUsr;
    }

}
