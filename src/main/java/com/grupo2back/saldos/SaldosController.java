package com.grupo2back.saldos;


import com.google.gson.Gson;
import com.grupo2back.movimientos.FiltroMovimiento;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/grupo2back")
public class SaldosController {
    @Autowired
    SaldoServiceEst saldoServiceEst;

/*    @GetMapping("/saldos")
    public List<Saldo> getSaldos(){
        return saldoServiceEst.findAll();
    }*/

    @GetMapping("/saldos")
    @ResponseBody
    public List getSaldos(@RequestParam String idUsr){
        FiltroSaldo filtroSaldo =new FiltroSaldo(idUsr);
        Gson gson=new Gson();
        String filtro=gson.toJson(filtroSaldo);

        return SaldosService.getFiltrado(filtro);
    }

    @PostMapping("/saldos")
    public String setServicio(@RequestBody String newSaldo){
        try{
            SaldosService.insert(newSaldo);
            return "Ok";
        }catch (Exception ex){
            return ex.getMessage();
        }
    }

    @PutMapping("/saldos")
    public String updServicios(@RequestBody String data){
        try{
            JSONObject obj=new JSONObject(data);
            String filtro= obj.getJSONObject("filtro").toString();
            String updates= obj.getJSONObject("updates").toString();
            SaldosService.update(filtro, updates);
            return "Ok";
        } catch (Exception ex){
            return ex.getMessage();
        }
    }
}
