package com.grupo2back.saldos;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface SaldoRepository extends MongoRepository<com.grupo2back.saldos.Saldo, String> {

}
