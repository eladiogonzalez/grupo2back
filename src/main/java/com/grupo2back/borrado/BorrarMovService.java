package com.grupo2back.borrado;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BorrarMovService {
    @Autowired
    Movimiento1Repository movimiento1Repository;




    // Borrar Movimiento Indicado
    public MovimientoModel delete(MovimientoModel  mov){
        System.out.println(mov);
        try {
            Optional<MovimientoModel> mov1 = movimiento1Repository.findById(mov.getId());

            movimiento1Repository.delete(mov);
            return mov1.get();
        } catch (Exception ex) {
            System.out.println(ex);
            return null;
        }
    }



    public List<MovimientoModel> findAll(){
        return movimiento1Repository.findAll();
    }



    public Optional<MovimientoModel> findById(String id){
        return movimiento1Repository.findById(id);
    }



}
