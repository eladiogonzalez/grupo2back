package com.grupo2back.borrado;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class InsertMovService {
    @Autowired
    HistoricoRepository historicoRepository;

    // incluir
    public boolean insertHistorico(HistoricoModel prod) {

        try {
            historicoRepository.save(prod);
            return true;
         } catch (Exception ex) {
             System.out.println(ex);
             return false;
         }


    }

    public List<HistoricoModel> findAll(){
        return historicoRepository.findAll();
    }



}
