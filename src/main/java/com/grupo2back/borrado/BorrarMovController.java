package com.grupo2back.borrado;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/g2")
public class BorrarMovController {
    @Autowired
    BorrarMovService borrarMovService;

    @Autowired
    InsertMovService insertMovService;

    @GetMapping("/movimiento")
    public List<MovimientoModel> getMovimientoModel(){
        List<MovimientoModel> movConsultar = borrarMovService.findAll();
        return movConsultar;
    }

    @GetMapping("/historico")
    public List<HistoricoModel> getHistoricoModel(){
        List<HistoricoModel> hisConsultar = insertMovService.findAll();
        return hisConsultar;
    }

    @DeleteMapping("/movimiento")
    public String delMovModel(@RequestBody MovimientoModel prod){
        MovimientoModel record = borrarMovService.delete(prod);
        if (record != null){
         if (insertMovService.insertHistorico(setHistoricoModel(record)))
             return ("Registro  " + record.getId() + "  transferido al Histórico");
        }
        //return null;
        return "Registro No Encontrado" ;

    }




    //funcion que invoca consulta de todos y filtra
    public MovimientoModel getMov( String prod){
    //public Object getMov( String prod){
      List<MovimientoModel> movConsultar = borrarMovService.findAll();
        for (MovimientoModel listMov:movConsultar){
            if (listMov.getId().equals(prod)){
                return listMov;
            }
        }
        return null;
    }

    public HistoricoModel setHistoricoModel(MovimientoModel param){
        HistoricoModel hist = new HistoricoModel();
        hist.setId(param.getId());
        hist.setIdMovimiento(param.getIdMovimiento());
        hist.setIdUsuario(param.getIdUsuario());
        hist.setFechaMovimiento(param.getFechaMovimiento());
        hist.setHoramovimiento(param.getHoramovimiento());
        hist.setDescripcion(param.getDescripcion());
        hist.setTipoMovimiento(param.getTipoMovimiento());
        hist.setStatus(param.getStatus());
        hist.setMonto(param.getMonto());
        return hist;
    }


}
