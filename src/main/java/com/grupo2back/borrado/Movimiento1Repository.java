package com.grupo2back.borrado;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

// interface para el movimiento repository
@Repository
public interface Movimiento1Repository extends MongoRepository<MovimientoModel, String>{

}
