package com.grupo2back.borrado;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

// interface para el repositorio historico
@Repository
public interface HistoricoRepository extends MongoRepository<HistoricoModel, String>{

}
