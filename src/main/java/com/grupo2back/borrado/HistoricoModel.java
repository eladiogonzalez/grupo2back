package com.grupo2back.borrado;


import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "historia")
public class HistoricoModel {
    @Id
    @NotNull
    private String id;
    private String idMovimiento;
    private String idUsuario;
    private String fechaMovimiento;
    private String horamovimiento;
    private String descripcion;
    private String tipoMovimiento;
    private String status;
    private Double monto;


    public HistoricoModel() {
    }

    public HistoricoModel(@NotNull String id, String idMovimiento, String idUsuario, String fechaMovimiento, String horamovimiento, String descripcion, String tipoMovimiento, String status, Double monto) {
        this.id = id;
        this.idMovimiento = idMovimiento;
        this.idUsuario = idUsuario;
        this.fechaMovimiento = fechaMovimiento;
        this.horamovimiento = horamovimiento;
        this.descripcion = descripcion;
        this.tipoMovimiento = tipoMovimiento;
        this.status = status;
        this.monto = monto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdMovimiento() {
        return idMovimiento;
    }

    public void setIdMovimiento(String idMovimiento) {
        this.idMovimiento = idMovimiento;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getFechaMovimiento() {
        return fechaMovimiento;
    }

    public void setFechaMovimiento(String fechaMovimiento) {
        this.fechaMovimiento = fechaMovimiento;
    }

    public String getHoramovimiento() {
        return horamovimiento;
    }

    public void setHoramovimiento(String horamovimiento) {
        this.horamovimiento = horamovimiento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }
}

