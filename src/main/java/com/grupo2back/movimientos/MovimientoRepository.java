package com.grupo2back.movimientos;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovimientoRepository extends MongoRepository<com.grupo2back.movimientos.Movimiento, String> {

}
