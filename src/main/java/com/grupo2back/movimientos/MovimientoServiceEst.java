package com.grupo2back.movimientos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MovimientoServiceEst {
    @Autowired
    MovimientoRepository movimientoRepository;

    public List<Movimiento> findAll(){
        return movimientoRepository.findAll();
    }

    public Optional<Movimiento> findById(String id){
        return movimientoRepository.findById(id);
    }

    public Movimiento save (Movimiento mov){
        return movimientoRepository.save(mov);
    }

    public boolean delete (Movimiento mov){
        try{
            movimientoRepository.delete(mov);
            return true;
        }catch (Exception ex){
            return false;
        }
    }
}
