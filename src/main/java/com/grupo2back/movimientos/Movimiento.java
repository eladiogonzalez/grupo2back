package com.grupo2back.movimientos;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "movimientos")
public class Movimiento {

    @Id
    private String idMovimiento;
    private String idUsuario;
    private String fechaMovimiento;
    private String horaMovimiento;
    private String descripcion;
    private String tipoMovimineto;
    private String status;
    private double monto;

    public Movimiento() {
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdMovimiento() {
        return idMovimiento;
    }

    public void setIdMovimiento(String idMovimiento) {
        this.idMovimiento = idMovimiento;
    }

    public String getFechaMovimiento() {
        return fechaMovimiento;
    }

    public void setFechaMovimiento(String fechaMovimiento) {
        this.fechaMovimiento = fechaMovimiento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipoMovimineto() {
        return tipoMovimineto;
    }

    public void setTipoMovimineto(String tipoMovimineto) {
        this.tipoMovimineto = tipoMovimineto;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getHoraMovimiento() {
        return horaMovimiento;
    }

    public void setHoraMovimiento(String horaMovimiento) {
        this.horaMovimiento = horaMovimiento;
    }

    @Override
    public String toString() {
        return "Movimiento{" +
                "idMovimiento='" + idMovimiento + '\'' +
                ", idUsuario='" + idUsuario + '\'' +
                ", fechaMovimiento='" + fechaMovimiento + '\'' +
                ", horaMovimiento='" + horaMovimiento + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", tipoMovimineto='" + tipoMovimineto + '\'' +
                ", status='" + status + '\'' +
                ", monto=" + monto +
                '}';
    }
}