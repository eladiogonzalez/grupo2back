package com.grupo2back.movimientos;

import com.google.gson.Gson;
import com.grupo2back.saldos.SaldosService;
import org.bson.Document;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/grupo2back")
public class MovimientoControler {



    @GetMapping("/movimientos")
    @ResponseBody
    public List getMovimientos(@RequestParam(required=false, defaultValue = "") String idMov, @RequestParam(required=false, defaultValue = "") String idUsr){
        FiltroMovimiento filtroMovimiento =new FiltroMovimiento(idMov, idUsr);
        Gson gson=new Gson();
        String filtro=gson.toJson(filtroMovimiento);

        return MovimientoService.getFiltrado(filtro);
    }


    @PostMapping("/movimientos")
    public String setMovimiento(@RequestBody String newMovimiento){
        try{
            SaldosService.insert(newMovimiento);
            return "Ok";
        }catch (Exception ex){
            return ex.getMessage();
        }

    }

//----------------------------
    @Autowired
    MovimientoServiceEst movimientoServiceEst;

    @PutMapping("/movimientos")
    public com.grupo2back.movimientos.Movimiento putMovimientos (@RequestBody com.grupo2back.movimientos.Movimiento mov){
        return  movimientoServiceEst.save(mov);
    }

    @DeleteMapping("/movimientos")
    public boolean delMovimientos (@RequestBody com.grupo2back.movimientos.Movimiento mov){
        return movimientoServiceEst.delete(mov);
    }
}
