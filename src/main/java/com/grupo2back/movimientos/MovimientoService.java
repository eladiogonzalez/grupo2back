package com.grupo2back.movimientos;

import com.grupo2back.saldos.SaldosService;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MovimientoService {
    static MongoCollection<Document> movimientos;

    private static MongoCollection<Document> getMovimientosCollection(){
        ConnectionString cs=new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        MongoClient mongo = MongoClients.create(settings);
        MongoDatabase database = mongo.getDatabase("dbmovibbva");
        return database.getCollection("movimientos");
    }

    public static void insert(String strMovimientos) throws Exception{

        movimientos=getMovimientosCollection();
        Document doc=Document.parse(strMovimientos);
        List<Document> lst =doc.getList("movimientos", Document.class);

        if(lst==null){
            //if (existeSaldoUsurio(strMovimientos) )
                movimientos.insertOne(doc);
        }else{
            movimientos.insertMany(lst);
        }
    }

    public static void insert2(String strMovimientos) throws Exception{
        movimientos=getMovimientosCollection();
        Document doc=Document.parse(strMovimientos);
        List<Document> lst =doc.getList("movimientos", Document.class);
        if(lst==null){
            movimientos.insertOne(doc);
        }else{
            movimientos.insertMany(lst);
        }
    }

    public static List getFiltrado(String filtro ){
        movimientos=getMovimientosCollection();
        List lst =new ArrayList();
        Document docFiltro=Document.parse(filtro);
        FindIterable<Document> iterDoc =movimientos.find(docFiltro);
        Iterator it = iterDoc.iterator();
        while (it.hasNext()){
            lst.add(it.next());
        }
        return lst;
    }

    public static boolean existeSaldoUsurio(String strFiltro ){
        movimientos=getMovimientosCollection();
        List lst =new ArrayList();
        Document docFiltro=Document.parse(strFiltro);
        FindIterable<Document> iterDoc =movimientos.find(docFiltro);
        Iterator it = iterDoc.iterator();
        while (it.hasNext()){
            lst.add(it.next());
        }
        if (lst == null){
            return true;
        }else return false;
    }

}
