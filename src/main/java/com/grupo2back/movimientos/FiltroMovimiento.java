package com.grupo2back.movimientos;

public class FiltroMovimiento {
    String idMovimiento;
    String idUsuario;

    public String getIdMovimiento() {
        return idMovimiento;
    }

    public void setIdMovimiento(String idMovimiento) {
        this.idMovimiento = idMovimiento;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public FiltroMovimiento(String idMovimiento, String idUsuario) {
        this.idMovimiento = idMovimiento;
        this.idUsuario = idUsuario;
    }

    public FiltroMovimiento() {
    }
}
